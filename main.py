
from flask import Flask,request,make_response,redirect, render_template
from flask.wrappers import Response #clase Flask de extension flask
from flask_sqlalchemy import SQLAlchemy
from werkzeug.wrappers import response
from Crud import Crud
import json #para poder dar formato json 
from datetime import date, datetime
import os

app = Flask(__name__) #instancia de Flask

#postgresql://<nombre_usuario>:<password>@<host>:<puerto>/<nombre_basededatos>
#app.config['SQLALCHEMY_DATABASE_URI'] ="postgresql://postgres:46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b@ec2-52-202-152-4.compute-1.amazonaws.com:5432/db6omgi47jan3t"
app.config['SQLALCHEMY_DATABASE_URI'] ="postgresql://eypvpsixrfnhzk:46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b@ec2-52-202-152-4.compute-1.amazonaws.com:5432/db6omgi47jan3t"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(32)

database = SQLAlchemy(app)

from modelo.tabla_usuarios import *


#código sprint 5-base de datos remota (heroku), salida a servidor remoto(heroku) y acceso a bases de datoS con ORM

#primer ruta 
@app.route('/')#ruta en el navegador
def hello():#funcion que se realiza cuando se acceda a esa ruta
    #user_ip = request.remote_addr # solicitando direccion ip al navegador (cliente)
    #print(user_ip)
    return render_template("1-login.html")

@app.route("/login", methods=["GET","POST"])
def login():
    nombre_usuario="admin"
    password="admin"
    if(request.method=="GET"):
        return render_template("1-login.html")
    if(request.method=="POST"):
        username=request.form.get("username")
        passwd = request.form.get("passwd")
        if(username==nombre_usuario and passwd==password):
            response = make_response(redirect("/portada"))
            return response
        else:
            return render_template("1-login.html",acceso="incorrecto")

@app.route("/portada", methods=["GET"])
def portada():
    if(request.method=="GET"):
            return render_template("2-portada.html")
    
@app.route("/catalogos", methods=["GET"])
def catalogos():
    if(request.method=="GET"):
            return render_template("3-catalogos.html")
    
@app.route("/usuarios", methods=["GET"])
def usuarios():
    if(request.method=="GET"):
            return render_template("4-usuarios.html")

@app.route("/ing_usuarios", methods=["GET","POST"])
def ing_usuarios():
    if(request.method=="GET"):
        return render_template("5-ing_usuarios.html")
    if(request.method=="POST"):
        nombres=request.form.get("nombres")
        apellidos = request.form.get("apellidos")
        tipo_ident= request.form.get("tipo_ident")
        numero_ident= request.form.get("numero_ident")
        telefono= request.form.get("telefono")
        direccion= request.form.get("direccion")
        email= request.form.get("email")
        rol_usuario= request.form.get("rol_usuario")
        
    usuario=Tabla_Usuarios(nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario)
    usuario.create()

    return render_template("5-ing_usuarios.html",ingreso="usuario ingresado") 
    
    
@app.route("/del_usuarios", methods=["GET","POST"])
def del_usuarios():
    if(request.method=="GET"):
        return render_template("6-del_usuarios.html")
    if(request.method=="POST"):
        id_usuario=request.form.get("id_usuario")
        usuario = Tabla_Usuarios.query.get(id_usuario)
        if (usuario):
            Tabla_Usuarios.delete(id_usuario)
            return render_template("6-del_usuarios.html",Sielimina="usuario eliminado")
        else:
            return render_template("6-del_usuarios.html",Noelimina="usuario no encontrado")


@app.route("/get_usuarios", methods=["GET"])
def get_usuarios():
    if(request.method=="GET"):
            return render_template("7-get_usuarios.html")


@app.route("/get_all_usuarios", methods=["GET"])
def get_all_usuarios():
    if(request.method=="GET"):
        return render_template("8-get_all_usuarios.html",usuarios = Tabla_Usuarios.get_all())


@app.route("/get_one_usuario", methods=["GET","POST"])
def get_one_usuario():
    if(request.method=="GET"):
        return render_template("9-get_one_usuario.html")
    if(request.method=="POST"):
        id_usuario=request.form.get("id_usuario")
        usuario = Tabla_Usuarios.query.get(id_usuario)
        if (usuario):
            return render_template("9-get_one_usuario.html",usuarios = Tabla_Usuarios.get_one(id_usuario))
        else:
            return render_template("9-get_one_usuario.html",Noelimina="usuario no encontrado")
        

@app.route("/up_usuario", methods=["GET","POST"])
def up_usuario():
    if(request.method=="GET"):
        return render_template("10-up_usuario.html")
    if(request.method=="POST"):
        id_usuario=request.form.get("id_usuario")
        usuario = Tabla_Usuarios.query.get(id_usuario)
        #ir_a_actualizar=request.form.get("actualizado")
        if (usuario):
            return render_template("10-up_usuario.html",usuarios = Tabla_Usuarios.get_one(id_usuario))
        else:
            return render_template("10-up_usuario.html",Noelimina="usuario no encontrado")
    response = make_response(redirect("/usuarios")) 
    return response


@app.route("/datos_up_usuario", methods=["GET","POST"])
def datos_up_usuario():
    if(request.method=="POST"):
        id_usuario=request.form.get("id_usuario")
        nombres=request.form.get("nombres")
        apellidos = request.form.get("apellidos")
        tipo_ident= request.form.get("tipo_ident")
        numero_ident= request.form.get("numero_ident")
        telefono= request.form.get("telefono")
        direccion= request.form.get("direccion")
        email= request.form.get("email")
        rol_usuario= request.form.get("rol_usuario")
        print(nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario)
        #usuario=Tabla_Usuarios(nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario)
        Tabla_Usuarios.update_usuario(id_usuario,nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario)
        return render_template("10-up_usuario.html",ingreso="usuario actualizado") 

        


#código sprint 4-base de datos remota (heroku) y acceso a bases de datos con SQL
"""
#leer un solo usuario
@app.route("/get_un_usuario")
def leer_un_usuario():
    crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    usuarios = crud.leer_un_usuario(numero_id)
    respuesta = []
    for registro in usuarios:
	    respuesta.append({"id_usuario":registro[0], "nombres":registro[1],"apellidos":registro[2],"tipo_ident":registro[3],"numero_ident":registro[4],"telefono":registro[5],"direccion":registro[6],"email":registro[7],"rol_usuario":registro[8]})
    respuestas=json.dumps(respuesta)
    print(respuestas)
    return respuestas 
 
#leer todos los usuarios
@app.route("/get_usuarios") 
def leer_usuarios():
    crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    usuarios = crud.leer_usuarios()
    respuesta = []
    for registro in usuarios:
	    respuesta.append({"id_usuario":registro[0], "nombres":registro[1],"apellidos":registro[2],"tipo_ident":registro[3],"numero_ident":registro[4],"telefono":registro[5],"direccion":registro[6],"email":registro[7],"rol_usuario":registro[8]})
    respuestas=json.dumps(respuesta)
    return respuestas 
   
#actualizar nombres 
@app.route("/up_nombres")
def actualizar_nombres():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    nombres=input("Ingrese nombres para actualizar: ")  
    My_crud.actualizar_nombres(numero_id,nombres)
    My_crud.cerrar()
    return "nombres actualizados"

#actualizar apellidos
@app.route("/up_apellidos")
def actualizar_apellidos():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    apellidos=input("Ingrese apellidos para actualizar: ")  
    My_crud.actualizar_apellidos(numero_id,apellidos)
    My_crud.cerrar()
    return "apellidos actualizados"

#actualizar tipo de identificacion
@app.route("/up_tipo_id")
def actualizar_tipo_ident():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    tipo_ident=input("Ingrese nuevo tipo de identificacion para actualizar: ")
    My_crud.actualizar_tipo_ident(numero_id,tipo_ident)
    My_crud.cerrar()
    return "tipo de identificacion actualizada"

#actualizar telefono
@app.route("/up_telefono")
def actualizar_telefono():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    telefono=input("Ingrese nuevo telefono para actualizar: ")
    My_crud.actualizar_telefono(numero_id,telefono)
    My_crud.cerrar()
    return "telefono actualizado"   

#actualizar direccion
@app.route("/up_direccion")
def actualizar_direccion():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    direccion=input("Ingrese nueva direccion para actualizar: ")
    My_crud.actualizar_direccion(numero_id,direccion)
    My_crud.cerrar()
    return "direccion actualizada"

#actualizar email
@app.route("/up_email")
def actualizar_email():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    email=input("Ingrese nuevo email para actualizar: ")
    My_crud.actualizar_email(numero_id,email)
    My_crud.cerrar()
    return "email actualizado"

#actualizar rol de usuario
@app.route("/up_rol")
def actualizar_rol_usuario():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    rol_usuario=input("Ingrese nuevo email para actualizar: ")
    My_crud.actualizar_rol_usuario(numero_id,rol_usuario)
    My_crud.cerrar()
    return "rol de usuario actualizado"

#actualizar numero de identificacion
@app.route("/up_numero_id")
def actualizar_numero_ident():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    id_usuario=input("Ingrese codigo id del usuario: ")
    numero_id=input("Ingrese nuevo numero de identificacion del usuario: ")
    My_crud.actualizar_numero_ident(id_usuario,numero_id)
    My_crud.cerrar()
    return "numero de identificacion actualizada"

#eliminar un usuario 
@app.route("/del_usuario")
def eliminar_usuario():
    My_crud = Crud("ec2-52-202-152-4.compute-1.amazonaws.com","db6omgi47jan3t","eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    numero_id=input("Ingrese numero de identificacion del usuario: ")
    My_crud.eliminar_usuario(numero_id)
    My_crud.cerrar()
    return "usuario eliminado"
""" 



if __name__=="__main__": #punto de partida, donde python empieza a ejecutar
	while(True):
		print("starting web server")
		app.run(debug=True,host='0.0.0.0')#arranca el servidor, 0.0.0.0->localhost
        #en el navegador se accede localhost:5000/