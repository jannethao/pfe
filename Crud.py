
from Conexion_db import Conexion_db


class Crud:
    def __init__(self,host,db,user,passwd):
        self.my_conn = Conexion_db(host,db,user,passwd)
        #creando un objeto de la clase conexion_db y definiendolo
        #como atributo de la clase Crud (self)
    
    #escribir metodos para escribir, leer, eliminar,actualizar registros de tablas
    #segun las necesidades de mi proyecto

    def insertar_usuario(self, nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario):
        query = "INSERT INTO \"Usuario\""+\
            "(nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario)"+\
                "VALUES ('"+nombres+"','"+apellidos+"','"+tipo_ident+"','"+numero_ident+"','"+telefono+"','"+direccion+"','"+email+"','"+rol_usuario+"')"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)
      
            

    def leer_usuarios(self): #los metodos llevan como parametro a self
        query = "SELECT id_usuario,nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario"+\
            " FROM \"Usuario\";"
        respuesta=self.my_conn.consultar_db(query)
        return respuesta

    def leer_un_usuario(self,numero_id): #los metodos llevan como parametro a self
        query = "SELECT id_usuario,nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario"+\
            " FROM \"Usuario\" WHERE numero_ident='"+numero_id+"';"
        respuesta=self.my_conn.consultar_db(query)
        return respuesta


    def actualizar_nombres(self,numero_id,nombres): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  nombres='"+nombres+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)

    def actualizar_apellidos(self,numero_id,apellidos): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  apellidos='"+apellidos+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)

    def actualizar_tipo_ident(self,numero_id,tipo_ident): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  tipo_ident='"+tipo_ident+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)  

    def actualizar_telefono(self,numero_id,telefono): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  telefono='"+telefono+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)           

    def actualizar_direccion(self,numero_id,direccion): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  direccion='"+direccion+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query) 

    def actualizar_email(self,numero_id,email): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  email='"+email+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query) 
    
    def actualizar_rol_usuario(self,numero_id,rol_usuario): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  rol_usuario='"+rol_usuario+"' WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)

    def actualizar_numero_ident(self,id_usuario,numero_id): #los metodos llevan como parametro a self
        query = "UPDATE \"Usuario\" SET  numero_ident='"+numero_id+"' WHERE id_usuario="+id_usuario+";"
        print(query)#for test purposes
        self.my_conn.escribir_db(query) 


    def eliminar_usuario(self,numero_id): #los metodos llevan como parametro a self
        query = "DELETE FROM \"Usuario\" WHERE numero_ident='"+numero_id+"';"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)



    def cerrar(self):
        self.my_conn.cerrar_db()