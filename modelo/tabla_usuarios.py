
from os import stat
from main import database

class Tabla_Usuarios(database.Model):
    __tablename__='Usuario'

    id_usuario = database.Column(database.Integer, primary_key=True)
    nombres = database.Column(database.String, nullable=False)
    apellidos = database.Column(database.String, nullable=False)
    tipo_ident = database.Column(database.String, nullable=False)
    numero_ident = database.Column(database.String, nullable=False)
    telefono = database.Column(database.String, nullable=False)
    direccion = database.Column(database.String, nullable=False)
    email = database.Column(database.String, nullable=False)
    rol_usuario = database.Column(database.String, nullable=False)



    def __init__(self, nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario):
        self.nombres=nombres
        self.apellidos=apellidos
        self.tipo_ident=tipo_ident
        self.numero_ident=numero_ident
        self.telefono=telefono
        self.direccion=direccion
        self.email=email
        self.rol_usuario=rol_usuario

    def create(self):
        database.session.add(self)
        database.session.commit()

    @staticmethod
    def delete(id_to_delete):
        Tabla_Usuarios.query.filter_by(id_usuario=id_to_delete).delete()
        database.session.commit()


    @staticmethod
    def get_all():
        return Tabla_Usuarios.query.order_by(Tabla_Usuarios.id_usuario.asc()).all()


    @staticmethod
    def get_one(id_a_mostrar):
        return Tabla_Usuarios.query.filter_by(id_usuario=id_a_mostrar)

    @staticmethod 
    def update_usuario(id_usuario,nombres,apellidos,tipo_ident,numero_ident,telefono,direccion,email,rol_usuario):
        print(id_usuario)
        usuario=Tabla_Usuarios.query.get(id_usuario)
        
        usuario.nombres=nombres
        usuario.apellidos=apellidos
        usuario.tipo_ident=tipo_ident
        usuario.numero_ident=numero_ident
        usuario.telefono=telefono
        usuario.direccion=direccion
        usuario.email=email
        usuario.rol_usuario=rol_usuario
        
        database.session.commit()